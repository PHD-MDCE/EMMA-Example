package Simple;

import Analysis.Metrics.MetricsAnalyzer;
import Analysis.Metrics.Packages.GenericMetrics;
import Analysis.Metrics.Persistence.DatabasePersistor;
import JoshUtils.Debugging.SysOutDebugger;
import JoshUtils.General.Configuration;
import Mining.Git.GitMiner;
import Simple.ExampleMetrics.ExampleMetrics;
import Utils.EMF.EMMAResourceSet;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.revwalk.RevCommit;

import java.io.IOException;
import java.sql.SQLException;

/**
 * Your config file should look something like this:
 * [Database]
 * url=jdbc:mysql://localhost/emma
 * username=YOUR_USERNAME_HERE
 * password=YOUR_PASSWORD_HERE
 * [DataRepository]
 * dir=./data/
 */
public class Example {

	/**
	 * @param repoPath    Path to the GIT repository
	 * @param datasetName The name you would like to give this dataset in the database
	 * @throws IOException
	 * @throws GitAPIException
	 * @throws SQLException
	 */
	public void run(String repoPath, String datasetName) throws IOException, GitAPIException, SQLException {

		/**
		 * ==========================================================================================
		 * Step 1: Prep your Analyses
		 * ==========================================================================================
		 */

		//Optional: load your config file
		Configuration.init("/Users/lordphnx/mdce.ini");

		//In this example, we will be persiting result ot the database. so tell the DBPersistor how to do that
		//If for some reason you cannoy use a DB, use the ugly SoutPersistor to get stuff on your Sys-Out
		//MetricsAnalyzer analyzer = new SoutPersistor();
		//you can create your own persistence methods by extending the "MetricsPersistenceAPI"
		DatabasePersistor db = new DatabasePersistor(
				Configuration.read("Database.url"), //feed it a JDBC compatible URL (e.g. jdbc:mysql://localhost/emma)
				Configuration.read("Database.username"), //feed it the DB username
				Configuration.read("Database.password") //feed it the DB password
		);

		//In this example we will only be extracting metrics using the MetricsAnalyzer
		//you can create your own analyzers by extending the "AnalyzerInterface"

		//We create a metricsAnalyzer, and tell it to persist the results using our DB Persistor
		MetricsAnalyzer analyzer = new MetricsAnalyzer(db);

		//Tell the analyzer under what dataset we should store the results
		analyzer.setDataset(datasetName);

		//In case of the MetricsAnalyzer, we have to tell it which metrics to pull
		analyzer.addMetricsPackage(new GenericMetrics());
		analyzer.addMetricsPackage(new ExampleMetrics());


		/**
		 * ==========================================================================================
		 * Step 1: Prep your data
		 * ==========================================================================================
		 */
		//In this example, we will be mining our data from a GIT repository
		GitMiner miner = new GitMiner();

		// optionally, you can automatically clone URLs to get the latest version
		// miner.clone("https://github.com/eclipse/ocl.git", "data/ocl");

		//To start, open the repository
		miner.openRepo(repoPath);


		//To do this, we will iterate every commit of the main branch
		SysOutDebugger.s_smartLog("Loading commit list...");
		for (RevCommit c : miner.getCommits()) {

			//First, check out the revision
			SysOutDebugger.s_smartLog("Checking out " + c.toString());
			miner.checkout(c);

			//==============================================================EMMA Resource Set (EMMA-RS) Instance
			//The EMMAResourceSet is an augmented version of EMF's ResourceSet. It gives you a few nice extra features
			EMMAResourceSet rs = new EMMAResourceSet();

			//==============================================================Tell EMMA-RS how to load various extensions
			//Note that for each file type you want to analyze, you will need to add the appropriate ResourceFactory
			//Advanced configuration
			rs.addResourceFactory("xmi", new XMIResourceFactoryImpl());
			rs.addResourceFactory("ecore", new XMIResourceFactoryImpl());
			//rs.addResourceFactory("qvt", new QvtOperationalResourceFactoryImpl());
			//rs.addResourceFactory("ocl", new CustomOCLResourceFactory(rs));

			//==============================================================Give EMMA knowledge of various metamodels
			//In order to correctly load everything. EMMA Will need to know about the metamodels for the various types of models.
			//Here you can
			rs.addMetaModel(EcorePackage.eINSTANCE);
			rs.addMetaModel(org.eclipse.ocl.ecore.EcorePackage.eINSTANCE);
			//rs.addMetaModel(org.eclipse.m2m.qvt.oml.);

			//==============================================================Add the files into the EMMA-RS
			//Emma has a convenience function to recursively add all files (that it can open) that recursively exist in a folder
			rs.addAll(repoPath);

			/**
			 * ==========================================================================================
			 * Step 3: Start your analyses
			 * ==========================================================================================
			 */
			//Tell the analyzer to analyze the newly filled ResourceSet
			//Note that you do not have to use an EMMAResourceSet, any EMF ResourceSet will do!
			analyzer.accept(rs);


		}

		//Always make sure to close you repository!!!
		miner.closeRepo();
	}


	public static void main(String[] args) {
		try {
			//new Test().run("/Users/lordphnx/IntelliJProjects/Emma2-Examples/data/repos/ocl");
			new Example().run("/Users/lordphnx/IntelliJProjects/Metrics/tempDir/carm2g", "Example-EMMA2");
		} catch (IOException | GitAPIException | SQLException e) {
			e.printStackTrace();
		}
	}

}
