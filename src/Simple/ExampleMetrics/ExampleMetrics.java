package Simple.ExampleMetrics;

import Analysis.AnalyzerImpls.MetricsPuller.core.Metric;
import Analysis.Metrics.Packages.MetricsPackage;
import org.eclipse.emf.ecore.EObject;

public class ExampleMetrics extends MetricsPackage {

	/**
	 * You can create new metrics by annotating any function in a MetricsPackag ewith the @Metric annotation
	 * The return type of the method must be int, float, double, boolean, or String
	 *
	 * @param obj The type of the parameter defines what objects the metric will be computed on. The framework will take care of dining them and doing so systematically
	 *
	 * @return
	 */
	@Metric(name="Example Metric")
	public int size(EObject obj){
		//return the number of child objects each object contains
		return obj.eContents().size();
	}


}
