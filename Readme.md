#EMMA Tutorial
##Introducton
Welcome to the EMMA tutorial.
EMMA is a framework for systematically processing EMF-based MDE data and analyzing its contents.

## Installation
To get started, import this folder into your favorite IDE.
We assume: 
* Java 1.8
* An SQL server (which we will connect to using JDBC)
* An SQL client
* Note that MySQL is prefered

To also use the EMMA frontend [https://gitlab.com/PHD-MDCE/EMMA-Frontend] you will also need:
* PHP 7 or higher

###The DataBase
After installing an SQL server, use the included emma.sql to create your schema

Set up the relevant credentials in your mdce.ini


##Components

EMMA Consists of several comoponents that each have their own responsibilities.

 * The GitMiner pulls relevant files from a Git repository into an intermediate file strucutre on disk. The location of this file strucutre is determined in your .ini file
 * On this intermediate file structure (usuallyn located in ./data) you can perform preprocessing (such as concrete to abstract syntax)
 * The DataRepository allows you to programatically access the intermediate file structure. 

###Configuration
The Configuration component is essentially a .ini-wrapper that allows static acces of other components to those settings 
All you will probably need is its initialization line:
``Configuration.init("path/to/file"")``

###Database

###DataRepository
Your ./data directory (Represented by the ``DataRepository`` class) is structured as follows:
* folder(dataset-name) (Represented by the ``DataSet`` class)
  * folder(file-name-1.ext1) is Represented by the ``HistoricalFile`` class
	* file(timestamp1.ext1) is Represented by the ``Revision`` class
	* file(timestamp2.ext1)
	* file(timestamp3.deleted) is represented by ``Revision`` objects with ``isDeleted() == true``
  * folder(file-name-2.ext2)
	* file(timestamp2.ext2)
	* file(timestamp3.ext2)
Your DataRepository allows you to programatically access there files:

``DataRepository.instance.getDataSet("dataset-name"")``

Will return the object representing the dataset.

``dataset.getHistoricalFile("file-name1.ext1")`` will return the Historical File for file-name-1, on which you could subsequently get any revision:
``hf.getRevisionAt(12345)`` using UNIX timestamp notation.


The nice thing about this intermediate format is that you can do any type of preprocessing on the file, as long as the structure and naming is honored.
 

  


###GitMiner(#GitMiner)

###Analyzers

####MetricsPuller
The MetricsPuller 

``

``

####Metrics-Over-Time

####Differences
Under Construction


## 