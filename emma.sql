create table configs
(
	config_id int auto_increment
		primary key,
	description text null,
	name text null,
	config text null
)
;

create table datasets
(
	dataset_id int auto_increment
		primary key,
	name varchar(256) default '__UNKNOWN__' null,
	repo varchar(256) default '__UNKNOWN__' null
)
;

create table differences
(
	id int auto_increment
		primary key,
	file_id int null,
	revision_from int null,
	revision_to int null,
	kind longtext null,
	parent longtext null,
	parent_type longtext null,
	feature longtext null,
	value longtext null,
	value_type longtext null
)
;

create table files
(
	file_id int auto_increment
		primary key,
	name longtext null,
	dataset_id int null
)
;

create table metrics
(
	id int auto_increment
		primary key,
	file_id int null,
	revision int null,
	object_id int null,
	metric varchar(64) null,
	value longtext null
)
;

create index metrics_file_id_index
	on metrics (file_id)
;

create index metrics_file_id_revision_index
	on metrics (file_id, revision)
;

create index metrics_metric_index
	on metrics (metric)
;

create index metrics_revision_index
	on metrics (revision)
;
